# Project overview

This script is a server script that listens for client requests on a given port and IP address. When it receives a request, 
it checks a file at a specified location for a given string, and responds to the client with either "STRING EXISTS" or 
"STRING NOT FOUND". It also logs information about the search query, the requesting IP address, the execution time, and the timestamp.


## Configuration:


The server's behavior can be configured by editing the config.ini file. The following options are available:

- `IP_ADDRESS`: The IP address that the server will listen on.
- `PORT`: The port that the server will listen on.
- `PATHS`: A dictionary of file paths. The server will look for the specified string in the file at the path specified by the 'linuxpath' key.
- `FLAGS`: A dictionary of flags that determine the server's behavior.
  - `REREAD_ON_QUERY`: If set to 'true', the server will re-read the file for each query. 
If set to 'false', the server will only read the file once at startup.

The MyTCPHandler class extends the socketserver.BaseRequestHandler class, which is a base class for implementing request handlers. 
It provides a number of methods that can be overridden to implement the server's behavior. In this case, the handle method is overridden 
to implement the server's logic for handling client requests.

In the handle method, the server first reads the path to the file from the configuration file using the configparser module. 
Then, it enters a loop to wait for and handle client requests. For each client request, the server checks the REREAD_ON_QUERY flag 
to determine whether to re-read the file. If the flag is set, the server opens the file and reads it line by line to search for 
the specified string. If the string is found, the server sends a response of "STRING EXISTS" to the client, otherwise it sends a 
response of "STRING NOT FOUND".

After sending the response, the server logs information about the search query, including the search query itself, the requesting IP address, 
the execution time, and the timestamp. The create_log_message function is used to generate the log message from the provided information.

The server can be run as a daemon by setting the DAEMON flag in the FLAGS section of the config.ini file to 'true'. When running as a daemon, 
the server will run in the background and will not terminate when the controlling terminal is closed. This can be useful for long-running servers 
that need to be able to continue running even if the controlling terminal is closed.


# Logging:


The server logs information about each search query to the standard output. The log messages have the following format:

DEBUG: search query = <search_query>, requesting IP = <requesting_ip>, execution time = <execution_time> ms, timestamp = <timestamp>




## Usage:

Follow this set to test this project

Step 1

Open your Linux terminal and navigate to any directory you want to clone your project

Step 2
Type git clone to your chosen directory:

```
git clone [repo_url here]

```

Step 3
Open your code with your favorite IDE in my case I am using VS code:

```
code .

```

Step 4 
Create your python virtual environment
```
python3 -m venv .venv
```

Step 5
Activate your virtual environment
```
source .venv/bin/active
```

Step6
Install requirements.txt to use the python packages I used for this project.
```
pip install -r requirements.txt
```

> `Note: make sure to include 200K.txt file to any location of your choice to your linux terminal.`

> `in my case in puth it in in this nginx path on linux terminal linuxpath = /var/www/html/200k.txt`

Step 7
To start the server, run the following command:
```
python server.py
```

Step 8
The server will listen for client requests on the IP address and port 
proceed to your linux terminal and type
```
telnet localhost 40000
```

Step 9
Example enter a string text:
```
Hello
```

If string match the search query you should get either 

`STRING EXISTS`

Or

`STRING NOT FOUND`


## General overview of this daemon Linux 

The unit file specifies the configuration for a Systemd service. 
The [Unit] section contains metadata about the service, including a description 
of the service and the dependencies of the service. 
The [Service] section specifies the command to be run to 
start the service and any additional options, such as the working directory and restart behavior. 
The [Install] section specifies when
and how the service should be installed.

The ExecStart line in the [Service] section specifies the command to be run to start the service. 
In this case, it specifies that the Python 
script at the specified file path should be run when the service is started. 
The Restart and RestartSec options in the [Service] section specify
that the service should be automatically restarted if it fails, 
with a delay of 120 seconds between restarts.

The WantedBy option in the [Install] section specifies that the service should be installed 
as a multi-user target, which means it will be started 
when the system is booted in multi-user mode.


# To Create the service to run the script in the background

Step 1
```
sudo nano /etc/systemd/system/as-service.service
```

Step 2
Copy the bash script in the root directory of this project name [as-service.service] 
to the newly created file located in [/etc/systemd/system/as-service.service]
this file can be edited with vm or nano on your linux terminal

Step 3 
Run this command 
```
sudo systemctl daemon-reload as-service 
```

Step 4
To Start the service with systemd 
```
sudo systemctl start as-service.service
```

Setp 5
To stop the service 
```
sudo systemctl stop as-service.service 
```

Step 6 
To enable the service 
```
sudo systemctl enable as-service.service
```

Step 7 
To disable the service
```
sudo systemctl disable as-service.service
```

Step 8
Check the status to see if our service is running
```
sudo systemctl status as-service.serveice
```

Step 9 
The server will listen for client requests on the IP address and port
```
telnet localhost 40000
```

Step 10
To restart the service run:
```
sudo systemctl restart as-service.service
```

Step 11
To Stop the service run:
```
sudo systemctl stop as-service.service 
```




