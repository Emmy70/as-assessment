import socket
import time
import pytest
import configparser

from server import generate_request, send_request


def test_performance():
    for file_size in range(10000, 1000000, 10000):
        try:
            # Generate the request
            request = generate_request(file_size)

            # Measure the execution time
            start_time = time.time()

            # Send the request to the server and receive the response
            response = send_request(request)

            # Measure the execution time
            execution_time = time.time() - start_time

            # Print the execution time
            print("File size: {}, Execution time: {:.3f} ms".format(file_size, execution_time))
        except socket.error as e:
            # Print an error message and continue to the next iteration if there is a network error
            print("ERROR: Network error - {}".format(e))
            continue

    for num_queries in range(1, 100):
        try:
            # Measure the execution time for a number of queries
            start_time = time.time()

            for i in range(num_queries):
                # Generate the request
                request = generate_request(10000)

                # Send the request to the server and receive the response
                response = send_request(request)
        except socket.error as e:
            # Print an error message and continue to the next iteration if there is a network error
            print("ERROR: Network error - {}".format(e))
            continue

        # Measure the execution time
        execution_time = time.time() - start_time

        # Calculate the average execution time per query
        avg_execution_time = execution_time / num_queries

        # Print the average execution time
        print("Number of queries: {}, Average execution time: {:.3f} ms".format(num_queries, avg_execution_time))


@pytest.mark.parametrize("file_size", [-1, 10000001])
def test_invalid_file_size(file_size):
    # Generate the request with an invalid file size
    request = generate_request(file_size)

    # Send the request to the server and receive the response
    response = send_request(request)

    # Verify that the response is an error message
    assert "ERROR" in response

@pytest.mark.parametrize("ip_address", ["invalid_ip", "256.256.256.256"])
def test_invalid_ip_address(ip_address):
    # Set the IP address of the client to an invalid value
    client_address = (ip_address, 0)

    # Generate the request
    request = generate_request(10000)

    # Send the request to the server and receive the response
    response = send_request(request)

    # Verify that the response is an error message
    assert "ERROR" in response

def test_file_not_found():
    # Set the path to the file to a non-existent file
    file_path = "invalid_file_path"

    # Update the configuration file with the new file path
    config = configparser.ConfigParser()
    config.read("config.ini")
    config.set("PATHS", "linuxpath", file_path)
    with open("config.ini", "w") as config_file:
        config.write(config_file)

    # Generate the request
    request = generate_request(10000)

    # Send the request to the server and receive the response
    response = send_request(request)

    # Verify that the response is an error message
    assert "ERROR" in response