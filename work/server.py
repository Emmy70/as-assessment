# Import the necessary modules
import configparser
import socketserver
import time
import sys
import socket

# import daemon


# Read the configuration file
config = configparser.ConfigParser()
config.read("config.ini")

def create_log_message(search_query, requesting_ip, execution_time, timestamp):
    """
    Create a log message with the specified information.

    :param str search_query: The search query received from the client.
    :param str requesting_ip: The IP address of the client that made the request.
    :param float execution_time: The time it took to execute the search query.
    :param float timestamp: The timestamp of the search query.
    :return: The log message.
    :rtype: str
    """
    return "DEBUG: search query = {}, requesting IP = {}, execution time = {:.3f} ms, timestamp = {}".format(
        search_query,
        requesting_ip,
        execution_time,
        timestamp
    )


def generate_request(file_size):
    """
    Generate a request with the specified file size.

    :param int file_size: The size of the file in bytes.
    :return: The request payload.
    :rtype: str
    """
    return "a" * file_size


def send_request(request):
    """
    Send a request to the server and return the response.

    :param str request: The request payload.
    :return: The response from the server.
    :rtype: str
    """
    # Connect to the server
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(('localhost', 5000))

        # Send the request to the server
        s.sendall(request.encode())

        # Receive the response from the server
        response = s.recv(1024).decode()
        
    return response
        
        

class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """


    def handle(self):
        # Get the path to the file from the configuartion file
        try:
            file_path = config.get("PATHS", "linuxpath")
        except config.Error:
            # Print an error message and exit the program if the path cannot be read from the configuration file
            print("ERROR: Failed to read the path to the file from the configuration file.")
            sys.exit(1)

        while True:
            # Check if the REREAD_ON_QUERY flag is set
            try:
                reread_on_query = config.get("FLAGS", "REREAD_ON_QUERY")
            except config.Error:
                # Print an error message and exit the program if the REREAD_ON_QUERY flag cannot be read from the configuration file
                print("ERROR: Failed to read the REREAD_ON_QUERY flag from the configuration file.")
                sys.exit(1)

            # Open the file at the specified path
            try:
                with open(file_path, mode='r') as file:
                    # Read the contents of the file line by line
                    for line in file:
                        # Check if the line contains the string
                        if "Hello" in line:
                            response = "STRING EXISTS\n"
                            break
                    else:
                        response = "STRING NOT FOUND\n"
            except IOError:
                # Print an error message and exit the program if the file cannot be opened at the specified path
                print("ERROR: Failed to open the file at the specified path {}".format(file_path))
                sys.exit(1)

            # self.request is the TCP socket connected to the client
            try:
                self.data = self.request.recv(1024).strip()
            except socket.error:
                # Print an error message and exit the program if data cannot be received from the client
                print("ERROR: Failed to receive data from the client.")
                sys.exit(1)

            print("{} wrote:".format(self.client_address[0]))
            print(self.data.decode('utf-8'))

            if len(self.data) > 1024:
                # Return an error message or take some other action
                pass


                # Strip any \x00 characters from the end of the payload
                self.data = self.data.strip('\x00')

            # just send back the same data in a string format
            try:
                self.request.sendall(response.encode('utf-8'))
            except socket.error:
                # Print an error message and exit the program if data cannot be sent to the client
                print("ERROR: Failed to send data to the client.")
                sys.exit(1)

           
            # Measure the execution time
            start_time = time.time()

            # Get the current timestamp
            timestamp = time.time()

            # Create a log message with the search query, the requesting IP, the measured
            # execution time, and the timestamp
            log_message = create_log_message(
                self.data.decode('utf-8'),
                self.client_address[0],
                (timestamp - start_time) * 1000,
                timestamp
            )
            print(log_message)

            # Print the log message to the console

       
           

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    """
    A threaded TCP server that uses the MyTCPHandler request handler class.
    """
    pass
    

if __name__ == "__main__":
    HOST, PORT = "localhost", 5000

    # Create the spiperver, binding to localhost on port 5000
    with ThreadedTCPServer((HOST, PORT), MyTCPHandler) as server:     
        # Activate the server; this will keep running until you
        # interrupt the program with Ctrl-C
        # Run the server as a daemon
        
        
        # daemon_context = daemon.DaemonContext()
        # daemon_context.open()
        server.serve_forever()
        

