import socketserver
import socket
from server import create_log_message, MyTCPHandler
import pytest 
import mock
import time 
# Import the timeit module
import timeit

@pytest.fixture
def mock_request():
    """
    Create a mock request object that simulates a client connection.
    """
    # Create a mock socket object
    mock_socket = mock.Mock(spec=socket.socket)

    # Set the data attribute of the mock socket object
    mock_socket.data = b"Test data"
    # Set the side effect of the recv method to return the data attribute
    mock_socket.recv.side_effect = lambda x: mock_socket.data

    # Return the mock socket object
    return mock_socket

HOST = "localhost"
PORT = 5000

def test_handle_success(mocker, mock_request):

    # Configure the self.data attribute of the mock request object
    mock_request.data= b"Test data"

    # Set the execution time of the _process_search_query() method
    mocker.patch.object(MyTCPHandler, "_process_search_query", return_value=b"SUCCESS\n")

    # Create an instance of the MyTCPHandler class
    handler = MyTCPHandler(mock_request, client_address=(HOST, PORT), 
    server=mock.Mock())

    # Invoke the handle() method
    handler.handle()

    # Verify that the request.sendall() method was called with the expected response
    mock_request.sendall.assert_called_once_with(b"SUCCESS\n")

    # Verify that the _log_request_response() method was called with the expected arguments
    mock_request._log_request_response.assert_called_once_with(b"", b"SUCCESS\n")

def test_handle_file_not_found(mocker, mock_request):
    # Set the execution time of the _process_search_query() method
    mocker.patch.object(
        MyTCPHandler, "_process_search_query", side_effect=FileNotFoundError
    )

    # Create an instance of the MyTCPHandler class
    handler = MyTCPHandler(mock_request, HOST, PORT)

    # Invoke the handle() method
    handler.handle()

    # Verify that the request.sendall() method was called with the expected response
    mock_request.sendall.assert_called_once_with(b"FILE NOT FOUND\n")

    # Verify that the _log_request_response() method was not called
    assert mock_request._log_request_response.call_count == 0

def test_handle_socket_error(mocker, mock_request):
    # Set the execution time of the _process_search_query() method
    mocker.patch.object(
        MyTCPHandler, "_process_search_query", side_effect=socket.error
    )

    # Create an instance of the MyTCPHandler class
    handler = MyTCPHandler(mock_request, HOST, PORT)

    # Invoke the handle() method
    handler.handle()

    # Verify that the request.sendall() method was called with the expected response
    mock_request.sendall.assert_called_once_with(b"SOCKET ERROR\n")
    
     # Verify that the _log_request_response() method was not called
    assert mock_request._log_request
    
def test_execution_time(mocker, mock_request):
    # Set the execution time of the _process_search_query() method
    mocker.patch.object(MyTCPHandler, "_process_search_query", return_value=b"SUCCESS\n")

    # Create an instance of the MyTCPHandler class
    handler = MyTCPHandler(mock_request, HOST, PORT)

    # Create a client to send search queries to the server
    client = socket.create_connection((HOST, PORT))

    # Test execution times for different file sizes
    for file_size in range(10000, 1000000, 10000):
        # Create a search query with the given file size
        search_query = b"Test query with file size " + str(file_size).encode()

        # Send the search query to the server
        client.sendall(search_query)

        # Measure the execution time of the handle() method
        execution_time = timeit.timeit(handler.handle, number=1)

        # Print the execution time and file size
        print("File size:", file_size, "| Execution time:", execution_time)

def test_queries_per_second(mocker, mock_request):
    # Set the execution time of the _process_search_query() method
    mocker.patch.object(MyTCPHandler, "_process_search_query", return_value=b"SUCCESS\n")

    # Create an instance of the MyTCPHandler class
    handler = MyTCPHandler(mock_request, HOST, PORT)

    # Create a client to send search queries to the server
    client = socket.create_connection((HOST, PORT))

    # Test execution times for different numbers of queries per second
    for queries_per_second in range(1, 100, 10):
        # Create a search query
        search_query = b"Test query"

        # Send the search query to the server multiple times
        start_time = time.time()
        for i in range(queries_per_second):
            client.sendall(search_query)
            handler.handle()
        end_time = time.time()

        # Calculate the total execution time
        execution_time = (end_time - start_time) * 1000

        # Print the execution time and number of queries per second
        print("Queries per second:", queries_per_second, "| Execution time:", execution_time)

def test_execution_time_vs_queries_per_second(mocker, mock_request):
    # Set the execution time of the _process_search_query() method
    mocker.patch.object(MyTCPHandler, "_process_search_query", return_value=b"SUCCESS\n")

    # Create an instance of the MyTCPHandler class
    handler = MyTCPHandler(mock_request, HOST, PORT)

    # Create a client to send search queries to the server
    client = socket.create_connection((HOST, PORT))

    # Test execution times for different file sizes and numbers of queries per second
    for file_size in range(10000, 1000000, 10000):
        for queries_per_second in range(1, 100, 10):
            # Create a search query with the given file size
            search_query = b"Test query with file size " + str(file_size).encode()

            # Send the search query to the server multiple times
            start_time = time.time()
            for i in range(queries_per_second):
                client.sendall(search_query)
                handler.handle()
            end_time = time.time()

            # Calculate the total execution time
            execution_time = (end_time - start_time) * 1000

            # Print the execution time, file size, and number of queries per second
            print(
                "File size:",
                file_size,
                "| Queries per second:",
                queries_per_second,
                "| Execution time:",
                execution_time,
            )

